# Database Migration Scripts
## Automation Steps
- This project contains all steps need to be taken in order to successfully deploy an Oracle Database into the OCI. 
- In order to make things easier we assume that both databases have connectivity with each other.
- all_in_one_selection_menu contains all steps (as functions).
- Prerequisites: 
        Public Keys from opc, oracle, grid user should be copied to the respective directories in target databases.

Steps:
<pre style="border: 1px solid black; padding: 10px;">

    1) OCI Module Installation: `step_1_object_storage_install_opc_install()`
    2a) Find the SCN Number: `step_2a_Automatic_SCN_Detection()`
    2) Object Storage Connection `step_2_object_storage_connection_take_a_backup()` 
    - Needs manually to be configured with:
        user_id='OracleIdentityCloudService/your-email-at-cern@cern.ch'
        auth_token='auth_token created by yourself under your profile at oracle cloud'
        bucket_name='pre-existing_bucket_name'
        wallet_dir='/home/oracle/hsbtwallet/'
        lib_dir='/home/oracle/lib'
        configfile='/home/oracle/config'
        host='https://swiftobjectstorage.eu-frankfurt-1.oraclecloud.com/v1/cernprod1'
    3) Create the pfile `step_3_pfile_creation()`    
    4)  Copy systemic files `step_4_Copy_System_Files()`
    5)  5a) Backup Handle Detection `step_5a_Automatic_Controlfile_Backup_Handle_Detection()` Automatically detects the SCN number and starts recovering to this point         
        5b) Find the DBID `step_5b_Select_DBID`
    6) Restore and Recovery of the Database: `step_6_Restore_n_Recover()` (Recover Database until a sequence number in order to keep both dbs in sync)  (`RESTORE DATABASE UNTIL "$scn_number" SEQUENCE;`)
    </pre>

    - All these steps are now gather in a single file called `menu.sh`  
    - On the target database it's needed to run this, in order to talk to the proper database (we overwrite the source db here)  
            `
            ORACLE_UNQNAME=db11_rhf_fra;  
            export ORACLE_UNQNAME  
            ORACLE_SID=db11  
            export ORACLE_SID  
           `
