#!/bin/bash

## Created by Alexandros Stoumpis alexandros.stoumpis@cern.ch ##

# TODO: add source paths global
# Let's Add some color.
RED='\033[0;31m'
GREEN='\033[0;32m'
BROWN='\033[1;33m'
BLUE='\033[0;34m'
MAGENTA='\033[0;35m'
CYAN='\033[0;36m'
BRIGHT_RED='\033[1;31m'
NC='\033[0m'

source /home/oracle/database-migration-scripts/all_in_one_selection_menu/object_storage_variables.sh 
export ORACLE_HOME=/u01/app/oracle/product/19.0.0.0/dbhome_1
#[ ! -f $CODE_PATH/"$(date +"%Y_%m_%d")_report.log"] && touch $CODE_PATH/"$(date +"%Y_%m_%d")_report.log"

general_output='Something is wrong with the logs...' #This shows up if logs went wrong...


display_menu()
{
    # Print a simple menu for choices
    echo -e "Choose an option:"
    echo -e "${BLUE}1. OCI Module Installation${NC}"
    echo -e "${GREEN}2a. Automatic SCN Detection${NC}"
    echo -e "${BRIGHT_RED}2. Connect Object Storage (OCI) & Take a backup${NC}"
    echo -e "${BLUE}3. Create the PFILE${NC}"    
    echo -e "${BROWN}4. Copy system files${NC}"
    echo -e "${GREEN}5a. Automatic Backup Handle Detection${NC}"
    echo -e "${GREEN}5b. Select DBID${NC}"
    echo -e "${BRIGHT_RED}6. DB Recovery & Restore${NC}"
    echo -e "${BROWN}0. Quit${NC}"
    echo -e "NOTE: DB & VM creation in OCI should be done manually, outside of this menu."
}

main()
{
    # Calls display_menu, take action upon choice
    while true; do
        display_menu
        read -p "Enter your choice: " choice
        case $choice in
            1)
                echo "You chose to run: step_1_object_storage_install_opc_install."
                step_1_object_storage_install_opc_install
		        
                ;;
            2a)
                echo "You chose to run the step 2a_Automatic_SCN_Detection This step takes long so please wait 30s+... "
                step_2a_Automatic_SCN_Detection
                 
                ;;
            2)
                echo "You chose to run: step_2_object_storage_connection_take_a_backup. This step takes a while 30s+..."
                step_2_object_storage_connection_take_a_backup
		        
                ;;
            3)
                echo "You chose to run: step_3_PFILE_creation"
                step_3_pfile_creation
                 
                ;;            
            4)
                echo "You chose to run: step_4_Copy_System_Files"
                step_4_Copy_System_Files
                 
                ;;
            5a)
                echo "You chose to run step_5a_Automatic_Controlfile_Backup_Handle_Detection "
                step_5a_Automatic_Controlfile_Backup_Handle_Detection
                 
                ;;
            5b)
                echo "You chose to run the step_5b_Select_DBID This step takes long so please wait 30s+... "
                step_5b_Select_DBID
                 
                ;;           
            6)
                echo "You chose to run the last step: step_6_Restore_n_Recover"
                step_6_Restore_n_Recover
                 
                ;;
            0)
                echo "Quitting..."
                exit; 
                ;;
            *)
                echo "Invalid choice. Please enter a number between 1 and 5."
                ;;
        esac
    done
}

cleaner_logs_rman()
{
    # It will filter out RMAN errors from general output
    # It will call json_builder, which store logs in json format

    rman_logfile="output.rman"
    # Search for the 4th occurrence of "RMAN" and capture text afterwards
    text1=$(grep -m 4 -o 'RMAN.*' $rman_logfile| awk 'NR==4')
    text2=$(grep -m 5 -o 'RMAN.*' $rman_logfile| awk 'NR==5')
    text3=$(grep -m 6 -o 'RMAN.*' $rman_logfile| awk 'NR==6')
    text4=$(grep -m 7 -o 'RMAN.*' $rman_logfile| awk 'NR==7')

    echo "$text1" >> "$(date +"%Y_%m_%d")_clean_report.log"
    echo "$text2" >> "$(date +"%Y_%m_%d")_clean_report.log"
    echo "$text3" >> "$(date +"%Y_%m_%d")_clean_report.log"
    echo "$text4" >> "$(date +"%Y_%m_%d")_clean_report.log"
    combined_text="${text1}\n${text2}\n${text3}\n${text4}"
    title=$text2
    log_level="ERROR"
    description=$combined_text

    json_builder    
}

cleaner_logs_ora()
{
    # Search for the occurrence of "ORA" and capture text afterwards
    # It will call json_builder, which store logs in json format

    ora_logfile="output.ora"
    title=$(grep -o 'ORA-.*' $ora_logfile)
    description=$title
    log_level="ERROR"
    echo "What is found using grep: $text1" # TODO: delete if not needed

    json_builder
}

cleaner_logs_success()
{
    # It will be called if no errors appear 
    # and put the successful message on json format logs
    # by calling json_builder

    echo "Command executed successfully."    
    log_level="SUCCESS"
    json_builder
}

json_builder()
{
    # It will be called and create logs in a json format
    # 
    # :param str description: It comes from caller. It will be added on logs file
    #
    ## Example of how it would look like:
    # {
    #   "datetime": "2024-07-09T12:50:15",
    #   "title": Title",
    #   "description": "description",
    #   "log_level": "SUCCESS/ERROR"
    # }
    datetime=$(date '+%Y-%m-%dT%H:%M:%S')

    output_file="$CODE_PATH/logs/json_log_$datetime.json"

    # Ensure the logs directory exists
    mkdir -p "$CODE_PATH/logs"

    #Ensure jsonlogfile exists
    [ ! -f "$output_file" ] && touch "$output_file"
    # Some jq tricks to build the file according to the following:
    json=$(jq -n \
    --arg dt "$datetime" \
    --arg tt "$title" \
    --arg em "$description" \
    --arg ll "$log_level" \
    '{datetime: $dt, title: $tt, log_level: $ll, description: $em}')
    # Write the JSON to a file
    echo "$json" > "$output_file"
    
    # The following commands are here to filter the passwords from the 
    # general output (written logs in the system)
    # sed -i "s/$password/PASSWORD/g" $output_file  
    # sed -i "s/$backup_password/BACKUP_PASSWORD/g" $output_file

    echo "JSON File in json_builder function is: $output_file"
    URL_HELP='https://docs.oracle.com/cd/E25054_01/backup.1111/e10642/rcmtroub.htm'
    echo -e "Here is a doc which might come handy with SQLplus/RMAN errors: ${BLUE}${URL_HELP}${NC}"

}

step_1_object_storage_install_opc_install()
{
    ### We use the following command syntax to install the backup module. 
    source $CODE_PATH/variables_initialization.sh
    source $CODE_PATH/object_storage_variables.sh
    mkdir -p $lib_dir   #It's required for opc_install.jar to run! 
    echo -e "You chose to run the java installer for Object Storage Connection ${GREEN}opc_install.jar.${NC}"
    cd /opt/oracle/oak/pkgrepos/oss/odbcs
    java -jar opc_install.jar -opcId $user_id -opcPass $auth_token -container $bucket_name -walletDir $wallet_dir -libDir $lib_dir -configfile $configfile -host $host 2>&1 | tee -a "$general_output"
    
    # Check if the BASH command was successful
    if [ "$exit_code" -eq 0 ]; then
        echo "BASH command executed successfully."
        description="$general_output"
        cleaner_logs_success
        
    elif [ "$exit_code" -eq 1 ]; then
        echo "BASH command failed with exit code 1."
        description="$general_output"       
    else
        echo "BASH command failed with an unexpected exit code $exit_code."
        description="$general_output"
    fi
}
step_2a_Automatic_SCN_Detection()
{
    source $CODE_PATH/variables_initialization.sh
    # Automatic SCN Detection Script (ASCNDS)
    # This function runs the 'SELECT' command to find the Current System Change Number
    # (This number constantly changes & indicates the current changes in the system )
    # We run this after complementary before running a specific command... 
    # ( RESTORE DATABASE UNTIL $scn_number SEQUENCE)

    set -e  # Exit immediately if a command exits with a non-zero status

    # Ensure the SCN output file exists
    filename="/tmp/scn.txt"
    [ ! -f "$filename" ] && touch "$filename"

    # Define RMAN command to find the current SCN
    #ALTER DATABASE MOUNT; --> might be needed in the future but not currently. To-Do: Delete.
    scn_find="
        connect target $username/$password@$db_unique_name
        SELECT CURRENT_SCN FROM V\$DATABASE;
    "

    # Run the RMAN command and capture output
    echo "Running RMAN command to find current SCN..."
    output_automatic_scn=$(echo "$scn_find" | rman 2>&1)  # Capture both stdout and stderr
    rman_exit_code=$?

    # Print the output for debugging
    echo "$output_automatic_scn"  # Print RMAN output to the screen

    description=$(echo "$output_automatic_scn" | tr -d '\n' | sed 's/"/\\"/g' | sed 's/[[:space:]]\+/ /g')

    # Check if the RMAN command was successful
    if [ $rman_exit_code -eq 0 ]; then
        echo "RMAN command executed successfully."

        # Extract SCN number with a regex for 6+ digit numbers
        scn_number=$(echo "$output_automatic_scn" | grep -o '[0-9]\{6,\}' | sed -n '2p')
        
        #echo "export scn_number=\"$scn_number\"" >> ~/.bashrc   # probably the wrong way to store it.
        echo "scn_number=$scn_number" >> details_recovery_db.sh    #appends the value in this file. We need this file to store the $DBID,$backup_handle,$scn
        # IT WORKS ^
        if [ -n "$scn_number" ]; then
            echo -e "${BROWN}SCN number is: $scn_number${NC}"
            echo "$scn_number" > "$filename"  # Save the SCN to the file
            cleaner_logs_success
        else
            echo "Failed to extract a valid SCN number."
            echo "RMAN output: $output_automatic_scn"  # Output for debugging
            cleaner_logs_rman
            exit 1
        fi
    elif [ $rman_exit_code -eq 1 ]; then
        echo "RMAN command failed with exit code 1."
        echo "RMAN output: $output_automatic_scn"  # Output for debugging
        cleaner_logs_rman
        exit 1
    else
        echo "Unexpected error: RMAN command failed with exit code $rman_exit_code."
        echo "RMAN output: $output_automatic_scn"  # Output for debugging
        exit $rman_exit_code
    fi
}

step_2_object_storage_connection_take_a_backup()
{
    # In this step we configure the database to be able to see the Object Storage.
    # Then we configure some RMAN options
    # Afterwards we take appropriate Backups 
    # At last, we show what backups we have
    set -e  # Exit immediately if any command exits with a non-zero status
    source $CODE_PATH/variables_initialization.sh
    source $CODE_PATH/object_storage_variables.sh 
    #cd /opt/oracle/oak/pkgrepos/oss/odbcs #let's comment it out. We probably don't need that.
    SOURCE_LIB='/u01/app/19.0.0.0/grid/lib'

    rman_commands="
        connect target $username/$password@$db_unique_name 
        CONFIGURE CHANNEL DEVICE TYPE 'SBT_TAPE' PARMS
        'SBT_LIBRARY=/home/oracle/lib/libopc.so,
        SBT_PARMS=(OPC_PFILE=/home/oracle/config)';
        CONFIGURE DEFAULT DEVICE TYPE TO SBT_TAPE;
        CONFIGURE BACKUP OPTIMIZATION ON;
        CONFIGURE CONTROLFILE AUTOBACKUP ON;
        CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE SBT_TAPE TO '%F';
        CONFIGURE ENCRYPTION FOR DATABASE ON;
        SET ENCRYPTION IDENTIFIED BY '$backup_password';  

        BACKUP INCREMENTAL LEVEL 0 SECTION SIZE 512M DATABASE PLUS ARCHIVELOG;
        BACKUP ARCHIVELOG ALL NOT BACKED UP 2 TIMES;

        LIST BACKUP OF ARCHIVELOG ALL;
        LIST BACKUP OF DATAFILE 1;
        LIST BACKUP OF CONTROLFILE;
        BACKUP CURRENT CONTROLFILE;
    "

    # Log the RMAN commands
    echo "$rman_commands" | tee -a "$CODE_PATH/$(date +"%Y_%m_%d")_report.log"

    # Execute RMAN commands and capture both output and exit code
    general_output=$(echo "$rman_commands" | rman)
    exit_code=$?

    # Display the RMAN output
    echo "${general_output}"

    # Check the exit code to determine if RMAN commands were successful
    if [ $exit_code -eq 0 ]; then
        echo "RMAN commands executed successfully."
        description=$general_output
        cleaner_logs_success
    elif [ $exit_code -eq 1 ]; then
        echo "RMAN commands failed with exit code 1."
        description=$general_output
        cleaner_logs_rman
    else
        echo "RMAN commands failed with an unexpected exit code $exit_code."
        description=$general_output
    fi
}

step_3_pfile_creation()
{
    #  Here we will create pfile from spfile (pure SQLplus called by bash script) 
    # and copy it (will be done in  step_4_Copy_System_Files) to the /tmp and then to the target server.
    set -e  # Exit on any command failure

    # Define SQLPlus command to create PFILE from SPFILE
    sqlplus_commands="CREATE PFILE='/tmp/pfile_test.ora' FROM SPFILE;"

    # Execute SQLPlus command and capture the output
    general_output=$(echo "$sqlplus_commands" | sqlplus / as sysdba)
    exit_code=$?

    # Remove newline characters from the output
    description=$(echo "$general_output" | tr -d '\n')
    echo "Description is: $description ... and exit code: $exit_code ..."

    # Check the exit status of the SQLPlus command
    if [ $exit_code -eq 0 ]; then
        echo "SQLPlus commands executed successfully."
        cleaner_logs_success
    elif [ $exit_code -eq 1 ]; then
        echo "SQLPlus commands failed with exit code 1."
        cleaner_logs_ora
    else
        echo "SQLPlus commands failed with an unexpected exit code $exit_code."
    fi
 
}

step_4_Copy_System_Files() 
{
    set -e  # Exit immediately if a command exits with a non-zero status

    # Load necessary environment variables
    source "$CODE_PATH/variables_initialization.sh"  # Sets username, password, and dbname
    source "$CODE_PATH/target_variables_initialization.sh"

    # Define paths for files and directories to be copied
    SOURCE_LIB='/home/oracle/lib'
    CONFIG_LOCATION='/home/oracle/config'
    ORACLE_WALLET="/opt/oracle/dcs/commonstore/wallets/$db_unique_name/"  # Ensure variable expansion
    PFILE='/tmp/pfile_test.ora'  # PFILE location to copy to the target server
    OCI_WALLET='/home/oracle/hsbtwallet/'
    target_server=$host  # Target server address

    # Use SCP to copy files and directories to the target server
    scp -o "ForwardAgent=yes" -r "$SOURCE_LIB" oracle@"$target_server":"$SOURCE_LIB"
    scp_exit_code=$?
    if [ $scp_exit_code -ne 0 ]; then
        echo "Failed to copy $SOURCE_LIB to $target_server. Exiting."
        exit $scp_exit_code
    fi

    scp -o "ForwardAgent=yes" -r "$CONFIG_LOCATION" oracle@"$target_server":"$CONFIG_LOCATION"
    scp_exit_code=$?
    if [ $scp_exit_code -ne 0 ]; then
        echo "Failed to copy $CONFIG_LOCATION to $target_server. Exiting."
        exit $scp_exit_code
    fi

    scp -o "ForwardAgent=yes" -r "$ORACLE_WALLET" oracle@"$target_server":"$ORACLE_WALLET"
    scp_exit_code=$?
    if [ $scp_exit_code -ne 0 ]; then
        echo "Failed to copy $ORACLE_WALLET to $target_server. Exiting."
        exit $scp_exit_code
    fi

    # Ensure PFILE exists before copying
    if [ ! -f "$PFILE" ]; then
        echo "PFILE not found at $PFILE. Exiting."
        exit 1
    fi

    scp -o "ForwardAgent=yes" -r "$PFILE" oracle@"$target_server":"$PFILE"
    scp_exit_code=$?
    if [ $scp_exit_code -ne 0 ]; then
        echo "Failed to copy $PFILE to $target_server. Exiting."
        exit $scp_exit_code
    fi

    scp -o "ForwardAgent=yes" -r "$OCI_WALLET" oracle@"$target_server":"$OCI_WALLET"
    scp_exit_code=$?
    if [ $scp_exit_code -ne 0 ]; then
        echo "Failed to copy $OCI_WALLET to $target_server. Exiting."
        exit $scp_exit_code
    fi

    echo "All files successfully copied to $target_server."

    #to-do: add path from passwordfile $ORACLE_HOME/dbs...
}

step_5a_Automatic_Controlfile_Backup_Handle_Detection()
{
    # This function is taking the output of the command `LIST BACKUP OF CONTROLFILE;` and tries to find the controlfile backup handle.
    # The handle is needed in the `restore_recovery_script.rman` in order to make `RESTORE CONTROLFILE FROM 'c-1713142806-20241015-08';(eg.) work.
    # We chose this over `RESTORE CONTROLFILE FROM AUTOBACKUP;` because it takes significantly less time to run. (RESTORE FROM... what? It takes more time.)
    #The rman command should run at the target machine (database)
    set -e  # Exit immediately if a command exits with a non-zero status

    # Load required variables
    source "$CODE_PATH/variables_initialization.sh"

    # Create temp file for storing RMAN command output if it doesn't already exist
    temp_file="/tmp/backup_handle_command.txt"
    [ ! -f "$temp_file" ] && touch "$temp_file"

    # Define RMAN commands to find the controlfile backup handle
    rman_commands="
        connect target $username/$password@$db_unique_name
        LIST BACKUP OF CONTROLFILE;
    "

    # Execute RMAN command and log output
    echo "$rman_commands" | rman | tee -a "$temp_file"
    rman_exit_code=$?

    # Check if RMAN command was successful
    if [ $rman_exit_code -ne 0 ]; then
        echo "RMAN command failed with exit code $rman_exit_code."
        cleaner_logs_rman
        exit $rman_exit_code
    fi

    # Extract the backup handle using regex
    backup_handle=$(tac "$temp_file" | grep -oE 'c-[0-9]+-[0-9]+-[0-9]+' | head -n 1)

    echo "backup_handle=$backup_handle" >> details_recovery_db.sh    #appends the value in this file. We need this file to store the $DBID,$backup_handle,$scn

    # Verify if a backup handle was found
    if [ -z "$backup_handle" ]; then
        echo "Failed to find a valid backup handle in $temp_file."
        cleaner_logs_ora
        exit 1
    else
        echo -e "${BROWN}Backup Handle is: $backup_handle ${NC}"
        description="$general_output"

    fi

    # Check if the RMAN command was successful
    if [ "$exit_code" -eq 0 ]; then
        echo "RMAN commands executed successfully."
        description="$general_output"
        cleaner_logs_success
    elif [ "$exit_code" -eq 1 ]; then
        echo "RMAN commands failed with exit code 1."
        description="$general_output"
        cleaner_logs_rman
    else
        echo "RMAN commands failed with an unexpected exit code $exit_code."
        description="$general_output"
    fi

}

step_5b_Select_DBID()
{
    # Sets clear the sqlplus output and gets the DBID from the database.  
    source $CODE_PATH/variables_initialization.sh 
    
    # SQL command to get the DBID
    DBID_QUERY="
        SET HEADING OFF;
        SET TERMOUT OFF;
        SET ECHO OFF;
        SELECT DBID FROM v\$database;
    "

    # Run the SQL command and capture the output
    DBID_OUTPUT=$(echo "$DBID_QUERY" | sqlplus -s "/ as sysdba")

    # Check if the SQL*Plus command was successful
    exit_code=$?
    echo "exit code is: $exit_code" # debug info

    if [ $exit_code -eq 0 ]; then
        echo "SQL*Plus commands executed successfully."
        
        # Extract the 10-digit DBID from the output
        clean_DBID=$(echo "$DBID_OUTPUT" | grep -o '[0-9]\{10\}')

        echo -e "The clean ${GREEN}DBID is:$clean_DBID${NC}"
        cleaner_logs_success
    elif [ $exit_code -eq 1 ]; then
        echo "SQL*Plus commands failed with exit code 1."
        cleaner_logs_ora
    else
        echo "Unexpected error: exit code= $exit_code."
    fi
    
    echo "$sqlplus_commands" | sqlplus -s "/ as sysdba" |tee -a $CODE_PATH/"$(date +"%Y_%m_%d")_report.log"
    # ^ This runs remotely not local
    ps -ef |grep pmon   #Debug command: To make sure the DB is up.
    echo $dbname

    echo "clean_DBID=$clean_DBID" >> details_recovery_db.sh    #appends the value in this file. We need this file to store the $DBID,$backup_handle,$scn
    
}  
step_6_Restore_n_Recover()
{
    # This function Restores from Controlfile and Recovers until given SCN. Further details in the restore_recovery_script.rman
    set -x  # Exit immediately if a command exits with a non-zero status

    # Load target database variables
    source "$CODE_PATH/target_variables_initialization.sh"
    source "$CODE_PATH/details_recovery_db.sh" # source the variables from the previous functions: $SCN,$Backup_Handle,$DBID
    
    # Define RMAN script and paths
    locate "restore_recovery_script.rman"
    rman_script="restore_recovery_script.rman"
    pfile_path='/tmp/pfile_test.ora'
    dbid=$clean_DBID
    decryption_password=$password
    controlfile_handle=$backup_handle
    scn=$scn_number

    # Export variables for substitution in RMAN script
    export PFILE_PATH="$pfile_path"
    export DBID="$dbid"
    export DECRYPTION_PASSWORD="$decryption_password"
    export CONTROLFILE_HANDLE="$controlfile_handle"
    export SCN="$scn"
    export clean_DBID
    export username
    export password
    export dbname

    # Debug information
    echo -e "${BLUE}Controlfile handle is: $CONTROLFILE_HANDLE${NC}"
    echo -e "${BROWN}SCN number is: $SCN${NC}"
    echo -e "${GREEN}DBID is: $clean_DBID${NC}"

    # Run RMAN script with substituted variables and log output
    general_output=$(envsubst < "$rman_script" | rman)
    exit_code=$?
    echo "Debug: RMAN command executed with exit code $?"

    # Log the RMAN command output to the daily log
    echo "$general_output" | tee -a "$CODE_PATH/$(date +"%Y_%m_%d")_report.log"

    # Check if the RMAN command was successful
    if [ "$exit_code" -eq 0 ]; then
        echo "RMAN commands executed successfully."
        description="$general_output"
        cleaner_logs_success
    elif [ "$exit_code" -eq 1 ]; then
        echo "RMAN commands failed with exit code 1."
        description="$general_output"
        cleaner_logs_rman
    else
        echo "RMAN commands failed with an unexpected exit code $exit_code."
        description="$general_output"
    fi

    # Additional logging for SCN report
    mkdir -p "/home/oracle/bin/gitlab/database-migration-scripts/logs/"
    echo "$general_output" | tee -a "/home/oracle/bin/gitlab/database-migration-scripts/logs/SCN_report.log"
}


main