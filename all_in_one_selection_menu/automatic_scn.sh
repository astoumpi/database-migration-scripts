# Let's Add some color.
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
MAGENTA='\033[0;35m'
CYAN='\033[0;36m'
BRIGHT_RED='\033[1;31m'
NC='\033[0m'
source /home/oracle/bin/gitlab/database-migration-scripts/all_in_one_selection_menu/variables_initialization.sh

Automatic_SCN_Detection()
    {
        # Automatic SCN Detection Script (ASCNDS)
        # This script runs the 'SELECT' command to find the Current System Change Number 
        #   (it shows the current change and constantly changes)
        # We run this after complementary after running a specific command...
        touch "/tmp/SCN_report.log"
        filename="/tmp/scn.txt"
        #filename="/home/oracle/bin/gitlab/database-migration-scripts/logs/SCN_report.log"
        regex_pattern="Control File Included: Ckp SCN: ([[:digit:]]+)"

        scn_find="
        connect target $username/$password@$db_unique_name
        ALTER DATABASE MOUNT;
        SELECT CURRENT_SCN FROM V\$DATABASE;
        "
        output_automatic_scn=$(echo "$scn_find" | rman)
        echo "$output_automatic_scn"
        echo "$scn_find" | tee -a "/tmp/SCN_report.log"
        while IFS= read -r line; do
            # Search for the pattern in the line and capture the content within parentheses
            if [[ "$line" =~ $regex_pattern ]]; then
                # Extract the captured content
                scn_number="${BASH_REMATCH[1]}"
                echo -e " ${GREEN}SUCCESS: $scn_number ${NC}" |tee -a /home/oracle/bin/gitlab/database-migration-scripts/logs/"$(date +"%Y_%m_%d")_report.log"
            fi
        done < "$filename"
        echo "scn_number is: $scn_number"
    }

Automatic_SCN_Detection
