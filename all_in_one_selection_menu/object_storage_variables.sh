#!/bin/bash
CODE_PATH='/home/oracle/database-migration-scripts/all_in_one_selection_menu'
user_id='OracleIdentityCloudService/alexandros.stoumpis@cern.ch'
auth_token='ermy4z9HAIs[wHI5cG-_'
bucket_name='bucket_backup_11_12'
wallet_dir='/home/oracle/hsbtwallet'
lib_dir='/home/oracle/lib'
configfile='/home/oracle/config'
host='https://swiftobjectstorage.eu-frankfurt-1.oraclecloud.com/v1/cernprod1'

# Example for reference:
# java -jar opc_install.jar -opcId $user_id -opcPass $auth_token -container $bucket_name; -walletDir ~/hsbtwallet/ -libDir ~/lib/ -configfile ~/config -host $hostname>
