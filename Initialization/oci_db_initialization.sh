# We can skip this step if we already have the database(s) prepared (through oci console)
#Step 0 : Creation of Databases
## You can either create the DBs by using this syntax OR 
## you can create a system by loading a JSON file.
oci db dbsystem create \
-- availability domain BKrI:EU-FRANKFURT-1-AD-3 \
-- cpu-core-count 4 \ 
-- data-storage-size-in-gbs 256 \
-- admin-password <password> \
-- display-name AlexPrimary03ManualDG \
-- db-workload OLTP   \                      # this one returned nothing in the query (why? )
-- licence-model  \                           # this one as well
-- db-version    \                                # neither this one is displayed
-- wait-for-state AVAILABLE \
