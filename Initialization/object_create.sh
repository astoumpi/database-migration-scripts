#!/bin/bash
## We run this in a system where oci-cli is installed. (laptop) Not on the server.


#NAMESPACE=$(oci os ns get --query 'data' --raw-output) #this is OPTIONAL
COMPARTMENT_OCID="ocid1.compartment.oc1..aaaaaaaao72wzwmexpujop3xxhz77bczh2i2gkypjhtavlhcv32two2v2nua" 
BUCKET_NAME="db_migration_01" 
LOG_FILE="../logs/bucket_creation.log"
touch $LOG_FILE
# Create the bucket
oci os bucket create --name $BUCKET_NAME --compartment-id $COMPARTMENT_OCID
echo "Bucket '$BUCKET_NAME' created successfully in Compartment ID: '$COMPARTMENT_OCID' ."| tee -a ../logs/bucket_creation.log
